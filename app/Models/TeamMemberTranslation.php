<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamMemberTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name',
        'position',
        'detail',
        'description'
    ];
}
