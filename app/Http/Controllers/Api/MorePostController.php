<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class MorePostController extends Controller
{
    public function morePosts(Request $request)
    {
        $dataOffset = $request->dataOffset;
        $locale = $request->get('locale');
        if(!$locale){
            $locale = App::getLocale();
        }
        $morePosts = Post::orderByDesc('created_at')
            ->published()
            ->skip($dataOffset)
            ->take(3)
            ->get();
        return view('partials/more-posts',[
            'posts' => $morePosts,
            'locale' => $locale
        ]);
    }
    public function moreNews(Request $request)
    {
        $dataOffset = $request->dataOffset;
        $locale = $request->get('locale');
        if(!$locale){
            $locale = App::getLocale();
        }
        $morePosts = News::orderByDesc('created_at')
            ->published()
            ->skip($dataOffset)
            ->take(3)
            ->get();
        return view('partials/more-news',[
            'news' => $morePosts,
            'locale' => $locale
        ]);
    }
}
