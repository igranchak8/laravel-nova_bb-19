<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\AssessmentsPage;
use App\Models\ContactPage;
use App\Models\DocumentResources;
use App\Models\GpaAgreementBlock;
use App\Models\GpaAgreementPage;
use App\Models\News;
use App\Models\NewsPage;
use App\Models\Post;
use App\Models\PrivacyPolicyPage;
use App\Models\ResourcesPage;
use App\Models\TaskforcePage;
use App\Models\TeamMember;
use App\Models\TechOperation;
use App\Models\TermsPage;

class PageController extends Controller
{
    public function gpa()
    {
        $content = GpaAgreementPage::firstOrFail();
        $blocks = GpaAgreementBlock::published()->orderBy('created_at')->get();

        return view('pages.gpa-agreement', compact('content', 'blocks'));
    }

    public function cooperation()
    {
        $content = TechOperation::firstOrFail();
        $dataOffset = 3;
        $count = Post::count();

        $posts = Post::orderByDesc('created_at')->published()->take($dataOffset)->get();

        return view(
            'pages.tech-cooperation',
            compact(
                'content',
                'posts',
                'dataOffset',
                'count'
            )
        );
    }

    // public function posts($post)
    // {
    //     $post = Post::where('slug', $post)->firstOrFail();

    //     return view('pages.post', compact('post'));
    // }

    public function assessments()
    {
        $content = AssessmentsPage::firstOrFail();

        return view('pages.assessments', compact('content'));
    }

    public function resources()
    {
        $content = ResourcesPage::firstOrFail();
        $documents = DocumentResources::published()->get();
        return view('pages.resources', compact('content', 'documents'));
    }

    public function news()
    {
        $content = NewsPage::firstOrFail();
        $dataOffset = 3;
        $count = News::count();

        $news = News::orderByDesc('created_at')->published()->take($dataOffset)->get();

        return view(
            'pages.news',
            compact(
                'content',
                'news',
                'dataOffset',
                'count'
            )
        );
    }

    public function privacy()
    {
        $content = PrivacyPolicyPage::firstOrFail();

        return view('pages.terms', compact('content'));
    }
    public function terms()
    {
        $content = TermsPage::firstOrFail();

        return view('pages.terms', compact('content'));
    }
    public function contacts()
    {
        $content = ContactPage::firstOrFail();

        return view('pages.contacts',  compact('content'));
    }
    public function taskforce()
    {
        $content = TaskforcePage::firstOrFail();

        $members = TeamMember::where('status', 1)->orderBy('sort')->get();

        return view('pages.taskforce', compact('content', 'members'));
    }
}
