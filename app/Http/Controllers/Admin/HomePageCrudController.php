<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\HomePageRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\App;

/**
 * Class HomePageCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class HomePageCrudController extends CrudController
{

    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\HomePage::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/homepage');
        CRUD::setEntityNameStrings('homepage', 'home page');
        $this->crud->denyAccess('delete');
        $this->crud->denyAccess('create');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setFromDb(); // fields

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(HomePageRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        CRUD::setValidation(HomePageRequest::class);
        $language = \Request::input('language') ?? 'en';
        App::setLocale($language);

        $this->crud->addField([
                                  'name' => 'language',
                                  'type' => 'hidden',
                                  'value' => App::getLocale(),
                              ]);
        $this->crud->addField([
                                  'name' => 'lang',
                                  'label' => 'Language',
                                  'type' => 'select_language',
                                  'value' => App::getLocale(),
                              ]);
        $this->crud->addFields([
                                    [
                                        'name' => 'title',
                                        'label' => 'Title',
                                        'type' => 'textarea',
                                    ],
                                    [
                                        'name' => 'first_block',
                                        'label' => 'First block',
                                        'type' => 'wysiwyg',
                                    ],
                                   [
                                       'name' => 'second_block',
                                       'label' => 'Second block',
                                       'type' => 'wysiwyg',
                                   ],
                                   [
                                       'name' => 'about_first_block_text',
                                       'label' => 'First block (About)',
                                       'type' => 'wysiwyg',
                                   ],
                                   [
                                       'name' => 'about_first_link_info',
                                       'label' => 'First link info',
                                       'type' => 'text',
                                   ],
                                   [
                                       'name' => 'about_first_link',
                                       'label' => 'First link',
                                       'type' => 'text',
                                   ],
                                   [
                                       'name' => 'about_second_block_text',
                                       'label' => 'Second block (About)',
                                       'type' => 'wysiwyg',
                                   ],
                                   [
                                       'name' => 'about_second_link_info',
                                       'label' => 'Second link info',
                                       'type' => 'text',
                                   ],
                                   [
                                       'name' => 'about_second_link',
                                       'label' => 'Second link',
                                       'type' => 'text',
                                   ]
                               ]);
    }
}
