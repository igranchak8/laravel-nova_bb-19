<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TeamMemberRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\App;

/**
 * Class TeamMemberCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TeamMemberCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\TeamMember::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/teammember');
        CRUD::setEntityNameStrings('teammember', 'team members');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumns([
                                    [
                                        'name' => 'name',
                                        'label' => 'Name',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'position',
                                        'label' => 'Position',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'status',
                                        'label' => "Status",
                                        'type' => 'select_from_array',
                                        'options' => [1 => 'Published', 0 => 'Unpublished'],
                                    ],
                                    [
                                        'name' => 'image',
                                        'label' => 'Image',
                                        'type' => 'image'
                                    ],
                                    [
                                        'name' => 'sort',
                                        'label' => 'Sort',
                                        'type' => 'number'
                                    ],
                                    [
                                        'name' => 'detail',
                                        'label' => 'Detail',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'description',
                                        'label' => 'Description',
                                        'type' => 'textarea'
                                    ],
                                ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(TeamMemberRequest::class);
        $language = \Request::input('language') ?? 'en';
        App::setLocale($language);

        $this->crud->addField([
                                  'name' => 'language',
                                  'type' => 'hidden',
                                  'value' => App::getLocale(),
                              ]);
        $this->crud->addField([
                                  'name' => 'lang',
                                  'label' => 'Language',
                                  'type' => 'select_language',
                                  'value' => App::getLocale(),
                              ]);
        $this->crud->addFields([
                                    [
                                        'name' => 'name',
                                        'label' => 'Name',
                                        'type' => 'text'
                                    ],
                                   [
                                       'name' => 'image',
                                       'label' => 'Image',
                                       'type' => 'image',
                                       'upload' => true,
                                       'crop' => true,
                                       'aspect_ratio' => 1.1
                                   ],
                                   [
                                       'name' => 'sort',
                                       'label' => 'Sort',
                                       'type' => 'number'
                                   ],
                                   [
                                       'name' => 'position',
                                       'label' => 'Position',
                                       'type' => 'text'
                                   ],
                                   [
                                       'name' => 'detail',
                                       'label' => 'Detail',
                                       'type' => 'text'
                                   ],
                                   [
                                       'name' => 'description',
                                       'label' => 'Description',
                                       'type' => 'wysiwyg'
                                   ],
                                   [
                                       'name' => 'status',
                                       'label' => "Status",
                                       'type' => 'select_from_array',
                                       'options' => [1 => 'Published', 0 => 'Unpublished'],
                                   ],
                               ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    protected function setupShowOperation()
    {
        $this->setupListOperation();
    }
}
