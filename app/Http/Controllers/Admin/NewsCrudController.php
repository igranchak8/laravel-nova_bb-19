<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\NewsRequest;
use App\Mail\UserRegister;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

/**
 * Class NewsCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class NewsCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\News::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/news');
        CRUD::setEntityNameStrings('news', 'news');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumns([
            [
                'name' => 'title',
                'label' => 'Title',
                'type' => 'text'
            ],
            [
                'name' => 'date',
                'label' => 'Date',
                'type' => 'text'
            ],
            [
                'name' => 'description',
                'label' => 'Description',
                'type' => 'wysiwyg'
            ],
            [
                'name' => 'image',
                'label' => 'Image',
                'type' => 'image'
            ],
            [
                'name' => 'link',
                'label' => 'Link',
                'type' => 'text'
            ],
            [
                'name' => 'status',
                'label' => "Status",
                'type' => 'select_from_array',
                'options' => [1 => 'Published', 0 => 'Unpublished'],
            ],
        ]);
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(NewsRequest::class);
        $language = \Request::input('language') ?? 'en';
        App::setLocale($language);

        $this->crud->addField([
            'name' => 'language',
            'type' => 'hidden',
            'value' => App::getLocale(),
        ]);
        $this->crud->addField([
            'name' => 'lang',
            'label' => 'Language',
            'type' => 'select_language',
            'value' => App::getLocale(),
        ]);
        $this->crud->addFields([
            [
                'name' => 'title',
                'label' => 'Title',
                'type' => 'text'
            ],
            [
                'name' => 'date',
                'label' => 'Date',
                'type' => 'text'
            ],
            [
                'name' => 'description',
                'label' => 'Description',
                'type' => 'wysiwyg'
            ],
            [
                'label' => "Image",
                'name' => "image",
                'type' => 'image',
                'upload' => true,
                'crop' => true,
                'aspect_ratio' => 1.5,
            ],
            [
                'name' => 'link',
                'label' => 'Link',
                'type' => 'text'
            ],
            [
                'name' => 'status',
                'label' => "Status",
                'type' => 'select_from_array',
                'options' => [1 => 'Published', 0 => 'Unpublished'],
            ],
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    protected function store()
    {
        $this->crud->hasAccessOrFail('create');
        $data = $this->crud->getStrippedSaveRequest();
        $data['name'] = $data['title'];
        $item = $this->crud->create($data);
        $item->save();
        $this->data['entry'] = $this->crud->entry = $item;
        \Alert::success(trans('backpack::crud.update_success'))->flash();
        $this->crud->setSaveAction();
        return $this->crud->performSaveAction($item->getKey());
    }
}
