<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Session\Session as SessionSession;
use Session;

class LanguageController extends Controller
{
    public function setLocale($locale) {
        Session::put('locale',$locale);
        return redirect()->back();
    }
}
