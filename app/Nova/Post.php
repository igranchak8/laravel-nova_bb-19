<?php

namespace App\Nova;

use Eminiarts\Tabs\Tabs;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\Status;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Slug;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Panel;
use phpDocumentor\Reflection\DocBlock\Tags\Author;
use SkoreLabs\NovaPanels\TabbedPanel;

class Post extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Post::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    public static $group = "Technical Operations";
    public static function label()
    {
        return 'Post';
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */



    public function fields(Request $request)
    {
        return [

            ID::make(__('ID'), 'id')->sortable(),
            Slug::make('Slug')->from('title'),
            Text::make('title_post')->rules('required', 'max:20')->default('BYE!!!!!!!!!!!!'),
            Image::make('main_image')->rules('required', 'image')->disk('public')
                ->path('storage_images'),
            Text::make('short_description')->rules('required', 'max:255')->default('Nothing!!'),
            Status::make('status'),
            BelongsTo::make("TechOperation"),


            Tabs::make(__('Client Custom Details'), [
                new Panel(__('FirstBlock'), [
                    ID::make('Id', 'id')->rules('required')->hideFromIndex(),
                    Text::make('Name', 'name')->default("Ivan"),
                    Text::make(__('Title'), 'title')->rules('max:255')->default("lalalalala"),
                    Trix::make('first_block_first_column')->rules('max:10000')->default("babababababa"),
                    Trix::make('first_block_second_column')->rules('max:10000')->default("ggggggggggggg"),
                ]),
                //HasMany::make('')
                new Panel(__('CentralBlock'), [
                    ID::make('Id', 'id')->rules('required')->hideFromIndex(),
                    Text::make('Name', 'name')->default("Abderahman"),
                    Text::make(__('Title'), 'title')->rules('max:255')->default("hdhdhdhdhdhdhdhd"),
                    Text::make(__('central_block_first_title'), 'title')->rules('max:255')->default("popopopopo"),
                    Image::make(__('First Item Image'), 'image'),
                    Trix::make('central_block_first_text')->rules('max:10000')->default("zzzzzzzzzzzzzzz"),
                    Text::make(__('central_block_second_title'), 'title')->rules('max:255')->default("gfgjhfkhsdkskd"),
                    
                    Image::make(__('Second Item Image'), 'image1'),

                    
                    Trix::make('central_block_second_text')->rules('max:10000')->default("nnnnnnnnnnnnnn"),
                    Text::make(__('central_block_third_title'), 'title')->rules('max:255')->default("vvvvvvvvv"),
                    Image::make(__('Third Item Image'), 'image2'),
                    Trix::make('central_block_third_text')->rules('max:10000')->default("kklolom;oilkkl"),
                ]),

                new Panel(__('LastBlock'), [
                    ID::make('Id', 'id')->rules('required')->hideFromIndex(),
                    Text::make('Name', 'name')->default("Alex"),
                    Trix::make('last_block_first_column')->rules( 'max:10000')->default("bubububububbubuu"),
                    Trix::make('last_block_second_column')->rules( 'max:10000')->default("tuuttutututututu"),

                ]),
            ]),

            // TabbedPanel::make(__('FirstBlock'), [
            //     'FirstTab' => [
            //       Text::make(__('Title'), 'title')->rules( 'max:255'),
            //       Trix::make('FirstColumn')->rules( 'max:10000'),
            //       Trix::make('SecondColumn')->rules( 'max:10000'),

            //     ]
            //   ]),

            //   TabbedPanel::make(__('CentralBlock'), [
            //     'FirstTab' => [
            //       Text::make(__('Title'), 'title')->rules( 'max:255'),
            //       Text::make(__('First Item Title'), 'title')->rules( 'max:255'),
            //       Image::make(__('First Item Image'), 'image'),
            //       Trix::make('First Item Text')->rules( 'max:10000'),
            //       Text::make(__('Second Item Title'), 'title')->rules( 'max:255'),
            //       Image::make(__('Second Item Image'), 'image'),
            //       Trix::make('Second Item Text')->rules( 'max:10000'),
            //       Text::make(__('Third Item Title'), 'title')->rules( 'max:255'),
            //       Image::make(__('Third Item Image'), 'image'),
            //       Trix::make('Third Item Text')->rules( 'max:10000'),



            //     ]
            //   ]),

            //   TabbedPanel::make(__('LastBlock'), [
            //     'FirstTab' => [
            //         Trix::make('FirstColumn')->rules( 'max:10000'),
            //         Trix::make('SecondColumn')->rules( 'max:10000'),

            //     ]
            //   ]),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
