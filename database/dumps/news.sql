INSERT IGNORE INTO `news` (`id`, `name`, `slug`, `image`, `link`, `status`, `created_at`, `updated_at`) VALUES
(1, 'news1', 'news1', '/storage/img/news-img-3.jpg', '', 1, '2020-10-07 06:00:01', '2020-10-07 11:12:09'),
(2, 'news2', 'news2', '/storage/img/news-img-3.jpg', '', 1, '2020-10-07 06:00:02', '2020-10-07 11:12:09'),
(3, 'news3', 'news3', '/storage/img/news-img-3.jpg', '', 1, '2020-10-07 06:00:03', '2020-10-07 11:12:09'),
(4, 'news4', 'news4', '/storage/img/news-img-3.jpg', '', 1, '2020-10-07 06:00:04', '2020-10-07 11:12:09'),
(5, 'news5', 'news5', '/storage/img/news-img-3.jpg', '', 1, '2020-10-07 06:00:05', '2020-10-07 11:12:09'),
(6, 'news6', 'news6', '/storage/img/news-img-3.jpg', '', 1, '2020-10-07 06:00:06', '2020-10-07 11:12:09'),
(7, 'news7', 'news7', '/storage/img/news-img-3.jpg', '', 1, '2020-10-07 06:00:07', '2020-10-07 11:12:09'),
(8, 'news8', 'news8', '/storage/img/news-img-3.jpg', '', 1, '2020-10-07 06:00:08', '2020-10-07 11:12:09'),
(9, 'news9', 'news9', '/storage/img/news-img-3.jpg', '', 1, '2020-10-07 06:00:09', '2020-10-07 11:12:09'),
(10, 'Project Title', 'project-title', '/storage/img/news-img-3.jpg', '', 1, '2020-10-07 06:00:26', '2020-10-07 11:12:09'),
(11, 'Project Title', 'project-title-1', '/storage/img/news-img-2.jpg', '', 1, '2020-10-07 06:00:27', '2020-10-07 11:12:09'),
(12, 'Far from gender balance?', 'far-from-gender-balance', '/storage/img/news-img-1.jpg', '', 1, '2020-10-07 06:00:28', '2020-10-07 11:12:09');
