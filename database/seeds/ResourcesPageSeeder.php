<?php

use Illuminate\Database\Seeder;

class ResourcesPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page = \App\Models\ResourcesPage::create([
                                                      'image' => '/storage/img/s-top-bg-4.jpg',
                                                      'slug' => 'resources'
                                                  ]);
        $title = 'Access publications, research papers and official documents related to the work of the EBRD GPA TC Facility.
Here you can also find the main legal texts that shape the work of the Facility.';
        $page->translateOrNew('en')->title = $title;
        $page->save();
    }
}
