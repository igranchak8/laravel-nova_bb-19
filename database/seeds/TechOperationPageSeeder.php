<?php

use Illuminate\Database\Seeder;

class TechOperationPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $page = \App\Models\TechOperation::create([
        //                                                  'image' => '/storage/img/map.svg',
        //                                                  'slug' => 'technical-operation'
        //                                              ]);
        // $title = 'Building on success';
        // $description = 'Out of the six countries that have become GPA Parties in the last decade, three gained accession with assistance from the EBRD: Montenegro in 2015, and Moldova and Ukraine in 2016. In addition, Armenia ratified the revised agreement in 2016 with support from the Bank. Currently, the EBRD GPA TC Facility is supporting the governments of North Macedonia, the Kyrgyz Republic and Tajikistan in preparing, negotiating and submitting to the Committee on Government Procurement an accession offer acceptable to all GPA Parties. ';
        // $first_block = '<p>
        //                 The EBRD, in cooperation with the WTO Secretariat, develops country-specific technical cooperation
        //                 projects, upon request of the interested governments, in alignment with the GPA standards.
        //                 Separately, the Bank’s experts provide non-binding legal assessments, including advice on compliance
        //                 of the national procurement legislation with the GPA mandatory requirements as well as assistance
        //                 with regulatory capacity building. The Bank has also helped countries negotiate the terms of their
        //                 GPA accession by identifying acceptable market access coverage offers, determining procurement
        //                 activities and/or entities to be covered by the Agreement and the possibility of implementing
        //                 special and differential treatment provisions (S&D) available to developing and least developed
        //                 economies acceding to the GPA, subject to negotiations with the GPA Parties.
        //             </p>';
        // $second_block = '<p>
        //                 The EBRD GPA TC Facility is available to all EBRD economies in the EBRD region wishing to join the
        //                 GPA. The Bank is currently assisting Azerbaijan, Belarus, North Macedonia, Georgia, Jordan,
        //                 Kazakhstan, the Kyrgyz Republic, Tajikistan and Turkey with their observers or accession requests
        //                 and corresponding negotiations as per the status of each economy. The Facility continues its
        //                 operation in over ten countries and is expanding its geographical reach. In addition to the work
        //                 under the Facility, the EBRD helps governments update their procurement legislation in line with the
        //                 Model Law on Public Procurement of the United Nations Commission on International Trade Law
        //                 (UNCITRAL), designed to be consistent with the GPA. More information on past and ongoing projects
        //                 can be found below.
        //             </p>';
        // $page->translateOrNew('en')->title = $title;
        // $page->translateOrNew('en')->description = $description;
        // $page->translateOrNew('en')->first_block = $first_block;
        // $page->translateOrNew('en')->second_block = $second_block;
        // $page->save();
    }
}
