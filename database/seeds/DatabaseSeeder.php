<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UserSeeder::class);
         $this->call(HomePageSeeder::class);
         $this->call(PrivacyPolicySeeder::class);
         $this->call(TaskforceSeeder::class);
         $this->call(GpaAgreementPageSeeder::class);
         $this->call(ResourcesPageSeeder::class);
         $this->call(TechOperationPageSeeder::class);
         $this->call(TermsSeeder::class);
         $this->call(GpaAgreementBlockSeeder::class);
         $this->call(PostsSeeder::class);
         $this->call(AssessmentSeeder::class);
         $this->call(DocumentResourcesSeeder::class);
         $this->call(NewsSeeder::class);
         $this->call(ContactSeeder::class);
         $this->call(TeamSeeder::class);
    }
}
