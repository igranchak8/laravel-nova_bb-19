<?php

use App\Models\NewsPage;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $images = ["storage_images/BXqMiKE04h2T9tN3mD5N4zKMil7rDjazweqcUBop.jpg", "storage_images/j3J3qLlnWPykIpSesPam4CGO1DML8HBDo2GfYYyK.jpg"];

        foreach ($images as $image) {
            $newPage = new NewsPage();
            $newPage->image = $image;
            $newPage->slug = Str::slug($image);
            $newPage->save();
        }
    }
}
