<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_translations', function (Blueprint $table) {
            $table->id();
            $table->string('locale')->index();
            $table->unsignedBigInteger('post_id');
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');

            $table->string('title');

            $table->string('first_block_title');
            $table->text('first_block_first_column');
            $table->text('first_block_second_column');

            $table->string('central_block_title');

            $table->string('central_block_first_title')->nullable();
            $table->text('central_block_first_text')->nullable();

            $table->string('central_block_second_title')->nullable();
            $table->text('central_block_second_text')->nullable();

            $table->string('central_block_third_title')->nullable();
            $table->text('central_block_third_text')->nullable();

            $table->string('last_block_title');
            $table->text('last_block_first_column');
            $table->text('last_block_second_column');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_translations');
    }
}
