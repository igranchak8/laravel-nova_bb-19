<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('slug');
            $table->string('title_post');
            $table->string('title');
            $table->string('short_description');
            $table->string('main_image');
            $table->string('first_block_first_column')->nullable();
            $table->string('first_block_second_column')->nullable();
            $table->string('first_block_third_column')->nullable();
            $table->string('central_block_first_text')->nullable();
            $table->string('central_block_second_text')->nullable();
            $table->string('central_block_third_text')->nullable();
            $table->string('last_block_first_column')->nullable();
            $table->string('last_block_second_column')->nullable();
            $table->string('central_block_first_image')->nullable();
            $table->string('central_block_second_image')->nullable();
            $table->string('central_block_third_image')->nullable();
            $table->foreignId('tech_operation_id');
            $table->boolean('status')->default(1);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
