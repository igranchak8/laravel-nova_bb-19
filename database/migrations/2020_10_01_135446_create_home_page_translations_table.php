<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomePageTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_page_translations', function (Blueprint $table) {
            $table->id();
            $table->string('locale')->index();
            $table->unsignedBigInteger('home_page_id');
            $table->foreign('home_page_id')->references('id')->on('home_page')->onDelete('cascade');
            $table->text('title');
            $table->text('first_block');
            $table->text('second_block');
//            $table->text('about_first_block_title');
//            $table->text('about_second_block_title');
            $table->text('about_first_block_text');
            $table->text('about_second_block_text');
            $table->string('about_first_link_info');
            $table->string('about_second_link_info');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_page_translations');
    }
}
