@extends('layouts.app')

@section('content')
    <section class="s-top" style="background-image: url({{ asset($content->image) }});">
        <div class="container">
            <h1 class="main-title">{{ (\Illuminate\Support\Facades\Route::currentRouteName() == 'privacy') ? __('content.privacy') : __('content.terms') }}</h1>
        </div>
    </section>

    <section class="s-terms">
        <div class="container">
            <div class="text-two-col">
                <div class="text-two-col__left">
                    {!! $content->first_block !!}
                </div>
                <div class="text-two-col__right">
                    {!! $content->second_block !!}
                </div>
            </div>
        </div>
    </section>
@endsection
