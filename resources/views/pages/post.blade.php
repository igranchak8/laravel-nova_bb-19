@extends('layouts.app')

@section('content')
    <section class="s-top s-top-single-blog" style="background-image: url(/{{ $post->image }});">
        <div class="container">
            <h1 class="main-title">{!! $post->title_post !!}</h1>
        </div>
        <a href="#s-single-blog-top" class="s-top-arrow"><img src="{{ asset('/storage/img/down-arrow-icon.svg') }}" alt=""></a>
    </section>

    <section class="s-single-blog-top" id="s-single-blog-top">
        <div class="container">
            <div class="blue-text">
                <p>
                    {!! $post->first_block_title !!}
                </p>
            </div>
            <div class="text-two-col">
                <div class="text-two-col__left">
                    {!! $post->first_block_first_column !!}
                </div>
                <div class="text-two-col__right">
                    {!! $post->first_block_second_column !!}
                </div>
            </div>
        </div>
    </section>

    <section class="s-single-blog">
        <div class="container">
            <h2 class="s-title">
                {!! $post->central_block_title !!}
            </h2>
        </div>
        <div class="s-blog-items">
            @if(!empty($post->central_block_first_text) || !empty($post->central_block_first_title))
                <div class="s-blog-item">
                    <div class="s-blog-item__text">
                        <h3 class="s-blog-item__title">{!! $post->central_block_first_title !!}</h3>
                        <div class="s-blog-item__hidden">
                            {!! $post->central_block_first_text !!}
                        </div>
                        <div class="s-blog-item__btn">
                            <div class="s-blog-item__read-more">{{ __('content.read_button') }}</div>
                        </div>
                    </div>
                    <div class="s-blog-item__img">
                        <img src="{{ asset($post->central_block_first_image) }}" alt="">
                    </div>
                </div>
            @endif
            @if(!empty($post->central_block_second_title) || !empty($post->central_block_second_text))
                <div class="s-blog-item">
                    <div class="s-blog-item__img">
                        <img src="{{ asset($post->central_block_second_image) }}" alt="">
                    </div>
                    <div class="s-blog-item__text">
                        <h3 class="s-blog-item__title">{!! $post->central_block_second_title !!}</h3>
                        <div class="s-blog-item__hidden">
                            {!! $post->central_block_second_text !!}
                        </div>
                        <div class="s-blog-item__btn">
                            <div class="s-blog-item__read-more">{{ __('content.read_button') }}</div>
                        </div>
                    </div>
                </div>
            @endif
                @if(!empty($post->central_block_third_title) || !empty($post->central_block_third_text))
                    <div class="s-blog-item">
                        <div class="s-blog-item__text">
                            <h3 class="s-blog-item__title">{!! $post->central_block_third_title !!}</h3>
                            <div class="s-blog-item__hidden">
                                {!! $post->central_block_third_text !!}
                            </div>
                            <div class="s-blog-item__btn">
                                <div class="s-blog-item__read-more">{{ __('content.read_button') }}</div>
                            </div>
                        </div>
                        <div class="s-blog-item__img">
                            <img src="{{ asset($post->central_block_third_image) }}" alt="">
                        </div>
                    </div>
                @endif
        </div>
    </section>

    <section class="s-single-blog-bottom">
        <div class="container">
            <div class="text-two-col">
                <div class="text-two-col__left">
                    {!! $post->last_block_first_column !!}
                </div>
                <div class="text-two-col__right">
                    {!! $post->last_block_second_column !!}
                </div>
            </div>
        </div>
    </section>
@endsection
