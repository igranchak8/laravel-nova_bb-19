@extends('layouts.app')

@section('content')
    <section class="s-top" style="background-image: url({{ asset($content->image) }});">
        <div class="container">
            <h1 class="main-title">{{ __('content.contact') }}</h1>
        </div>
    </section>

    <section class="s-contact">
        <div class="container">
            <form id="contact-form" method="POST" action="" class="contact-form">
                @csrf
                <div class="form-row">
                    <div class="form-col-6">
                        <div class="form-group">
                            <div class="form-label">{{ __('content.first_name') }}</div>
                            <input minlength="3" type="text" name="name" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-col-6">
                        <div class="form-group">
                            <div class="form-label">{{ __('content.email_address') }}</div>
                            <input type="email" name="email" class="form-control" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-label">{{ __('content.your_enquiry') }}</div>
                    <textarea minlength="5" name="enquiry" class="form-control textarea" required></textarea>
                </div>
                <div class="btn-wrap">
                    <button type="submit" form="contact-form" class="btn btn-blue">{{ __('content.send') }}</button>
                </div>
            </form>
        </div>
    </section>
    <div class="popup-overlay"></div>
    <div class="popup-wrap">
        <div class="popup popup-congrut">
            <div class="popup-close"><img src="{{ asset('/storage/img/close-icon.svg') }}" alt=""></div>
            <div class="popup-congrut__message success-mail">{{ __('content.mail_sent') }}</div>
            <div class="popup-congrut__message error-mail">{{ __('content.mail_error') }}</div>
        </div>
    </div>
@endsection
