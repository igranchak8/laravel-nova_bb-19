<input type="hidden" name="{{ $field['name'] }}">

<div id="app">
    <survey-results :results="{{ $entry[$field['name']] }}" :template="{{$entry['survey_template_id']}}"></survey-results>
</div>

@push('crud_fields_scripts')
    <script src="{{ asset('js/app.js') }}" defer></script>
@endpush

@push('crud_fields_styles')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style media="screen">
      .sv-panel__content {
        margin-left: 0px;
      }
    </style>
@endpush
