<div class="popup-wrap">
    <div class="popup popup-person">
        <div class="popup-close"><img src="{{ asset('/storage/img/close-icon.svg') }}" alt=""></div>
        <div class="popup-person__wrap">
            <div class="popup-person__left">
                <img src="{{ $member->image }}" alt="">
            </div>
            <div class="popup-person__right">
                <div class="popup-person__name">{{ $member->name }}</div>
                <div class="popup-person__details">European Bank for Reconstruction and Development</div>
                <div class="popup-person__position">{{ $member->position }}</div>
                <div class="popup-person__descr">
                    {{ $member->description }}
                </div>
            </div>
        </div>
    </div>
</div>
