import $ from "jquery";

document.addEventListener("DOMContentLoaded", function() {

	$(".s-top-arrow").click(function () {
	    var scroll_el = $(this).attr("href");
	    if ($(scroll_el).length != 0) {
	        $("html, body").animate({
	            scrollTop: $(scroll_el).offset().top
	        }, 500);
	    }
	    return false;
	});


	$(".contact-form").on("submit", function (e) {
	    e.preventDefault();
        $.ajax({
            method: "POST",
            url: "/api/send-mail",
            data: $(this).serialize()
        }).done(function(data) {
            $("body").addClass("overflow");
            $(".popup-overlay").addClass("active");
            $(".popup-wrap").addClass("active");
            if(data.status === 'success'){
                $(".success-mail").css('display', 'block');
                $(".error-mail").css('display', 'none');
                $("input[name='name']").val('');
                $("input[name='email']").val('');
                $("textarea[name='enquiry']").val('');
            } else {
                $(".success-mail").css('display', 'none');
                $(".error-mail").css('display', 'block');
            }
        }).fail(function() {
            $("body").addClass("overflow");
                    $(".popup-overlay").addClass("active");
                    $(".popup-wrap").addClass("active");
                    $(".error-mail").css('display', 'block');
                    $(".success-mail").css('display', 'none');
        });
        setTimeout(function () {
            $(".popup-overlay").removeClass("active");
            $(".popup-wrap").removeClass("active");
            $("body").removeClass("overflow");
        }, 4500);
	});

	$(".s-blog-item__read-more").on("click", function () {
	    $(this).hide();
	    $(this).closest(".s-blog-item__text").find(".s-blog-item__hidden").addClass("active");
	});


	$(".hamburger").click(function () {
	    $(this).toggleClass("is-active");
	    $(".top-header__right").toggleClass("active");
	});

	$(".main-menu__caret").on("click", function () {
	    $(this).toggleClass("active");
	    $(this).closest("li").find(".sub-menu").toggleClass("active");
	});


	$(".accordion-title").click(function () {
	    var blockFaq = $(this).parents(".accordion-item");
	    if (!$(blockFaq).hasClass("accordion-active")) {
	        $(".accordion-item").removeClass("accordion-active");
	        $(".accordion-content").slideUp(300);
	        $(blockFaq).addClass("accordion-active");
	        $(blockFaq).find(".accordion-content").slideDown(300);
	    } else {
	        $(".accordion-item").removeClass("accordion-active");
	        $(".accordion-content").slideUp(300);
	    }
	});


	$(".person-item__img, .person-item__name, .person-item__arrow").on("click", function (e) {
	    e.preventDefault();
	    $("body").addClass("overflow");
	    $(".popup-overlay").addClass("active");
	    $(".popup-wrap").addClass("active");
	});

	$(".popup-close").on("click", function () {
	    $(".popup-overlay").removeClass("active");
	    $(".popup-wrap").removeClass("active");
	    $("body").removeClass("overflow");
	});

	$(document).on("mouseup", function (e) {

	    var target = e.target;
	    var targetPopup = $(".popup");

	    if (!targetPopup.is(target) && targetPopup.has(target).length === 0) {
	        $(".popup-overlay").removeClass("active");
	        $(".popup-wrap").removeClass("active");
	        $("body").removeClass("overflow");
	    }

	});

	let popups = document.querySelectorAll(".popup");

	function checkPopupsOffset() {
	    for (let index = 0; index < popups.length; index++) {
	        let cfgPopupHeight = popups[index].offsetHeight;
	        let intViewportHeight = window.innerHeight - 100;

	        if (intViewportHeight <= cfgPopupHeight) {
	            popups[index].classList.add("popup-top");
	        } else {
	            popups[index].classList.remove("popup-top");
	        }
	    }
	}
    //
	// checkPopupsOffset();
    //
	window.addEventListener('resize', function (event) {
	    checkPopupsOffset();
	});
});
